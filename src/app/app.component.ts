import { EmployeeService } from './employee.service';
import { Employee } from './employee';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'employeemanagerapp';
  public employees: Employee[] = [];
  public employee: Employee | undefined;
  public editEmployee: Employee | undefined;
  public deleteEmployee: Employee | undefined;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.getEmployees();
  }

  public getEmployees(): void {
    this.employeeService.getEmployees().subscribe((response: Employee[]) => {
      this.employees = response;
    }, (error: HttpErrorResponse) => {
      alert(error.message);
    }

    );
  }



  public onOpenModal(employee: Employee | undefined, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      this.editEmployee = employee;
      button.setAttribute('data-target', '#addEmployeeModal');
    }
    if (mode === 'edit') {
      this.editEmployee = employee;
      button.setAttribute('data-target', '#updateEmployeeModal');
    }
    if (mode === 'delete') {
      this.deleteEmployee = employee;
      button.setAttribute('data-target', '#deleteEmployeeModal');
    }
    container?.appendChild(button);
    button.click();
    button.setAttribute('hidden', 'true')
  }


  public addEmployee(employeeForm: NgForm): void {
    document.getElementById('addFormId')?.click();
    console.log('Adding employee...', employeeForm.value);
    this.employeeService.addEmployees(employeeForm.value).subscribe(
      (response: Employee) => {
        console.log(response);
        this.employee = response;
        this.getEmployees();
        employeeForm.reset();
      },
      (error: HttpErrorResponse) => {
        console.error(error);
        employeeForm.reset();
      }
    );
  }
  public updateEmployee(employee: Employee | undefined): void {
    console.log(`Editing employee...${employee}`);
    this.employeeService.updateEmployees(employee).subscribe(
      (response: Employee) => {
        console.log(response);
        this.employee = response;
        this.getEmployees();
      },
      (error: HttpErrorResponse) => {
        console.error(error);
      }
    );
  }

  public onDeleteEmployee(employeeId: number | undefined): void {
    console.log(`Deleting employee by id: ${employeeId}`);
    this.employeeService.deleteEmployees(employeeId).subscribe(
      (response: Employee) => {
        console.log(`Employee deleted`);
        this.employee = response;
        this.getEmployees();
      },
      (error: HttpErrorResponse) => {
        console.error(error);
      }
    );
  }

  public searchEmployees(key: string): void {
    console.log('Searching employees...');
    const results: Employee[] = [];
    for (const employee of this.employees) {
      if (employee.name.toLowerCase().indexOf(key.toLowerCase()) !== -1
        || employee.email.toLowerCase().indexOf(key.toLowerCase()) !== -1 ||
        employee.phone.toLowerCase().indexOf(key.toLowerCase()) !== -1 ||
        employee.jobTitle.toLowerCase().indexOf(key.toLowerCase()) !== -1) {
        results.push(employee);
      }
    }
    this.employees = results;
    if (results.length === 0 || !key) {
      this.getEmployees();
    }
  }



}
